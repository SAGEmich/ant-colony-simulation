Ant Colony Simulation is my first big solo project. 
I would like to intrduce you to my research and source code of it.

 Ant colony optimization(ACO) was first introduced by Marco Dorigo in the 90s in his Ph.D. thesis. This program simualtes behaviours of real-life ants searching for food and taking it to the colony. Initially, it was used to solve the well-known traveling salesman problem. Ants are social isects, just like bees. While searching, ants roam randomly looking for food or pheromones left by other ants. Pheromones are natural signs left on the ground by other ants. There are two kinds of pheromones. Pheromones "to food" and "to home". While searching for food, ants leave "to home" pheromones on the ground and when they find food, ants leave "to food" pheromones. 

 I want to keep it as simple as posiible for better readibility. 
